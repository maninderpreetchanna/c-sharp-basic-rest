﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Http;

namespace appointmentService.Controllers
{
    public class TestController : ApiController
    {
        // GET: Test
        public IEnumerable<string> Get()
        {
            return new string[] { "t1", "t2" };
        }
    }
}